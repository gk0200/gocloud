import { RouterProvider, createBrowserRouter } from 'react-router-dom';
import './App.css';
import Home from './components/Home';
import Products from './components/Products';
import About from './components/About';
import Contact from './components/Contact';
import Blogs from './components/Blogs';
import Error from './components/Error';


function App() {
  const appRouter = createBrowserRouter([

    {
      path:"/",
      element:<Home/>
    },
    {
      path:"/products",
      element:<Products/>
    },
    {
      path:"/about",
      element:<About/>
    },
    {
      path:"/contact",
      element:<Contact/>
    },
    {
      path:"/blogs",
      element:<Blogs/>
    },
    {
      path:"*",
      element : <Error/>
    }
  ],
)

  return (
    <div>
      <RouterProvider router={appRouter}/>
    </div>

  );
}

export default App;
