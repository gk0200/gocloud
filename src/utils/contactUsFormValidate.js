const checkValidateData = (firstName,email,message) =>{
    const isEmailValid = /^([a-zA-Z0-9._%-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,})$/.test(email)
    const isFirstNameValid = /^[a-zA-Z\s]+$/.test(firstName)
    const isMessageValid = /^\s*$/.test(message)
    if(!isEmailValid) return "**Email ID is not valid"
    if(isMessageValid) return "**Fill the Message "
    if(!isFirstNameValid) return "**Name is not valid"
    return null
}

export default checkValidateData