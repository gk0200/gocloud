import { Link } from "react-router-dom";
import { FiMenu } from "react-icons/fi";
import { IoClose } from "react-icons/io5";
import { FaCaretDown } from "react-icons/fa";
import { FaCaretUp } from "react-icons/fa";

import { useState } from "react";
// import ThemeButton from "./ThemeButton";
// import { useRecoilValue } from "recoil";
// import { themeAtom } from "../store/atoms";

export default function Header(){
    const [open,setOpen] = useState(false)
    // const darkTheme = useRecoilValue(themeAtom)
    return(
        <div >
            <nav className="fixed-top bg-white shadow-md dark:bg-[#808080]  shadow-cyan-500/50  dark:shadow-red-900 z-30">
                <div className="flex items-center font-medium justify-around">
                    <div className="z-50 md:w-auto w-full flex justify-between ">
                        <Link to="/"><img className="md:cursor-pointer h-16" src="https://res.cloudinary.com/dqi0szfm7/image/upload/v1699933781/uzsb0jwypigpyjkukump.png" alt="logo"/></Link>
                        <div className="text-3xl md:hidden" onClick={()=>setOpen((prev)=>!prev)}>
                            {open?<IoClose/>:<FiMenu/>}
                        </div>
                    </div>
                    <ul className="md:flex hidden uppercase items-center gap-8 font-[Poppins]">
                        <li className="border-4 border-transparent hover:border-[rgba(0,0,0,0)] hover:bg-[rgba(0,0,0,0)] hover:rounded-lg">
                            <Link to="/products" className="px-3 inline-block ">Products</Link>
                        </li>
                        <Navlink/>
                        <li>
                            <Link to="/about" className=" px-3 inline-block">About</Link>
                        </li>
                        <li>
                            <Link to="/blogs" className=" px-3 inline-block">Blogs</Link>
                        </li>
                        <li>
                            <Link to="/contact" className=" px-3 inline-block">Contact Us</Link>
                        </li>
                        <li className="flex">
                            <Link to="/cart" className=" px-3 inline-block flex">
                                
                                <div>
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 28 33" width="28" height="33">
                                <path d="M24.5 6.6L21 6.6C21 2.9535 17.8675 0 14 0C10.1325 0 7 2.9535 7 6.6L3.5 6.6C1.575 6.6 0 8.085 0 9.9L0 29.7C0 31.515 1.575 33 3.5 33L24.5 33C26.425 33 28 31.515 28 29.7L28 9.9C28 8.085 26.425 6.6 24.5 6.6ZM14 3.3C15.925 3.3 17.5 4.785 17.5 6.6L10.5 6.6C10.5 4.785 12.075 3.3 14 3.3ZM24.5 29.7L3.5 29.7L3.5 9.9L7 9.9L7 13.2C7 14.1075 7.7875 14.85 8.75 14.85C9.7125 14.85 10.5 14.1075 10.5 13.2L10.5 9.9L17.5 9.9L17.5 13.2C17.5 14.1075 18.2875 14.85 19.25 14.85C20.2125 14.85 21 14.1075 21 13.2L21 9.9L24.5 9.9L24.5 29.7Z" fill="rgba(46,63,152,1)" fill-rule="nonzero" />
                                </svg>
                                </div>
                                <div className=" rounded-full mr-4">
                                    (0)
                                </div>
                                
                            </Link>
                        </li>
                    </ul>
                    <div className="md:block hidden">
                        {/* <ThemeButton/> */}
                    </div>
                    {/* Mobile nav */}
                    <ul className={`z-30 md:hidden bg-white absolute w-full h-full bottom-0 pt-10 pl-4 duration-500 ${open?"left-0":"left-[-100%]"}`}>
                        <li>
                            <Link to="/" className="py-4 px-3 inline-block">Home</Link>
                        </li>
                        <Navlink/>
                        <li>
                            <Link to="/about" className="py-4 px-3 inline-block">About</Link>
                        </li>
                        <li>
                            <Link to="/blogs" className="py-4 px-3 inline-block">Blogs</Link>
                        </li>
                        <li>
                            <Link to="/contact" className="py-4 px-3 inline-block">Contact Us</Link>
                        </li>
                        <li>
                            <Link to="/cart" className="py-4 px-3 inline-block">Cart</Link>
                        </li>
                        <div className="">
                            {/* <ThemeButton/> */}
                        </div>
                    </ul>
                </div>
            </nav>
        </div>
    )
}

const Navlink = () =>{
    const links = [
        {
            name:"For Business",
            submenu:true,
            sublinks:[
            {
                Head:'Links',
                sublink:[
                    {name:'Link-1',link:"/"},
                    {name:'Link-2',link:"/"},
                    {name:'Link-2',link:"/"}
                ]
            }
            ]
        },
    ]

    const [heading,setHeading] = useState("")
    const [subHeading,setSubHeading] = useState("")
    return(
        <>
            {
                links.map((link)=>{
                    return(
                        <div >
                            <div className="px-3 text-left md:cursor-pointer group">
                                <h1 className="flex justify-between items-center md:pr-0 pr-5 group" onClick={()=>
                                    {heading!==link.name?setHeading(link.name):setHeading("");setSubHeading("")}}>
                                    {link.name}
                                    <span className="text-xl md:hidden inline">
                                        {heading=== link.name?<FaCaretUp/>:<FaCaretDown/>}
                                    </span>
                                    <span className="text-xl md:mt-1 md:ml-2 hidden md:block group-hover:rotate-180 group-hover:-mt-2">
                                        <FaCaretDown/>
                                    </span>
                                </h1>
                                
                                {link.submenu && (
                                    <div>
                                        <div className="absolute z-50 top-[40px] hidden group-hover:md:block hover:md:block">
                                            <div className="py-3">
                                                <div className="w-4 h-4 left-3 absolute mt-1 bg-cyan-500/50 rotate-45">
                                                </div>    
                                            </div>
                                            <div className="bg-cyan-500 p-3.5">
                                                {
                                                link.sublinks.map((mysublinks)=>(
                                                    <div className="p-2" >
                                                        <h1 className="text-lg font-semibold">{mysublinks.Head}</h1>
                                                        {mysublinks.sublink.map((link)=>(
                                                            <li  className="text-sm text-gray-600 my-2.5">
                                                                <Link to={link.link} className="hover:text-red-600">
                                                                    {link.name}
                                                                </Link>
                                                            </li>
                                                        ))}
                                                    </div>
                                                )
                                                )
                                                }
                                            </div>
                                        </div>     
                                    </div>   
                                )}
                            </div>
                            {/**Mobile menus */}
                            <div className={`${heading===link.name ? 'md:hidden':'hidden'} z-20`}>
                                {/**sublinks */}
                                {
                                    link?.sublinks?.map((slink)=>(
                                        <div>
                                            <div>
                                                <h1 
                                                    className="py-4 pl-7 font-semibold md:pr-0 pr-5 flex justify-between items-center" 
                                                    onClick={()=>
                                                        subHeading!==slink.Head ? setSubHeading(slink.Head):setSubHeading("")}>
                                                            {slink.Head}
                                                            <span className="text-xl md:mt-1 md:ml-2 inline">
                                                                {subHeading=== slink.Head?<FaCaretUp/>:<FaCaretDown/>}
                                                            </span>
                                                </h1>
                                                <div className={`${subHeading===slink.Head ?'md:hidden' :'hidden'}`}>
                                                    {slink.sublink.map(slink=>(
                                                        <li className="py-3 pl-14">
                                                            <Link to={slink.link} className="hover:text-primary">{slink.name}</Link>
                                                        </li>
                                                    ))}
                                                </div>
                                            </div>
                                        </div>
                                    ))
                                }
                            </div>
                        </div>        
                    )
                })
            }
        </>
    )
}
