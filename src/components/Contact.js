import Header from "./Header";
import { SlSocialInstagram } from "react-icons/sl";
import { RiTwitterXFill } from "react-icons/ri";
import { useRef, useState } from "react";
import checkValidateData from "../utils/contactUsFormValidate";

export default function Contact(){
    const [errMsg,setErrMsg]=useState(null)
    const firstName = useRef(null)
    const lastName = useRef(null)
    const email = useRef(null)
    const message = useRef(null)

    const submitForm = () =>{
        console.log({firstName:firstName.current.value,lastName:lastName.current.value,email:email.current.value,message:message.current.value})

        const validate=checkValidateData(firstName.current.value,email.current.value,message.current.value)
        console.log(validate)
        setErrMsg(validate)
        if(validate) return;
        //API Call
        firstName.current.value="";
        lastName.current.value="";
        email.current.value="";
        message.current.value="";
    }
    return(
        <div>
            <Header/>
            <div className="flex justify-center items-center my-1">
                <form onSubmit={(e)=>e.preventDefault()} className="flex  gap-6 flex-col w-[400px] p-8 bg-gray-100 rounded-sm ">
                    <div className="flex gap-6 flex-row w-full">
                        <input
                        type="text"
                        className="flex-grow flex-shrink-0 p-2 w-0 h-10 bg-white border border-gray-300 rounded-md"
                        placeholder="First name"
                        ref={firstName}
                        />
                        <input
                        type="text"
                        className="flex-grow flex-shrink-0 p-2 w-0 h-10 bg-white border border-gray-300 rounded-md"
                        placeholder="Last name"
                        ref={lastName}
                        />
                    </div>
                    <input
                        type="email"
                        className="w-full h-10 mt-4 p-2 bg-white border border-gray-300 rounded-md"
                        placeholder="Email address"
                        ref={email}
                    />
                    <textarea
                        className="w-full h-32 mt-4 p-2 bg-white border border-gray-300 rounded-md"
                        placeholder="Message"
                        ref={message}
                    ></textarea>
                    <label className="flex items-center mt-4">
                        <input type="checkbox" className="w-5 h-5 mr-2" />
                        <span>You agree to our Privacy Policy</span>
                    </label>
                    <button
                        onClick={submitForm}
                        className="w-full h-10 mt-4 text-white bg-blue-500 rounded-md"
                    >
                        Send message
                    </button>
                    {errMsg!==null?<h1 className="font-bold text-red-600">{errMsg}</h1>:""}
                    <div className="flex items-center justify-center mt-4" >
                    message    <div className="flex gap-2 cursor-pointer">
                            <div className="mt-1">
                            <SlSocialInstagram/>
                            </div>
                            
                        <span>Instagram</span>
                        </div>
                        <div className="flex gap-2 ml-4 cursor-pointer">
                            <div className="mt-1">
                                <RiTwitterXFill/>
                            </div>
                        <span >Twitter</span>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    )
}